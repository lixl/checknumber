#include <stdio.h>
enum { T=13, F=14 };
char M[13][12] = {
	/*0 17 89 ad  e  f  x +- ' ' .  \0 F*/
	{ 1, 7, 7, F, F, F ,F ,5, 0,11, F, F},
	{ 6, 6, F, F, F, F, 2, F, 4,11, T, F},
	{ 3, 3, 3, 3, 3, 3, F, F, F, F, F, F},
	{ 3, 3, 3, 3, 3, 3, F, F, 4, F, T, F},
	{ F, F, F, F, F, F, F, F, 4, F, T, F},
	{ 7, 7, 7, F, F, F, F, F, F, 7, F, F},
	{ 6, 6, F, F, F, F, F, F, 4, F, T, F},
	{ 7, 7, 7, F, 8, F, F, F, 4,11, T, F},
	{ 9, 9, 9, F, F, F, F,10, F, F, F, F},
	{ 9, 9, 9, F, F, F, F, F, 4, F, T, F},
	{ 9, 9, 9, F, F, F, F, F, F, F, F, F},
	{12,12,12, F, F, F, F, F, F, F, F, F},
	{12,12,12, F, 8, F, F, F, 4, F, T, F}
};

char E(char c) {
	switch(c) {
		case '0': return 0;
		case '8':
		case '9': return 2;
		case 'a':
		case 'b':
		case 'c':
		case 'd': return 3;
		case 'e': return 4;
		case 'f': return 5;
		case 'x': return 6;
		case '+':
		case '-': return 7;
		case ' ': return 8;
		case '.': return 9;
		case '\n':
		case EOF:return 10;
		default: if(c>='1' && c<='7') return 1;
	}
	return 11;
}

int main() {
	char c, s[64], *p, state = 0;
	do {
		c = getchar();
		state =  M[state][E(c)];
		if(c == '\n') {
			printf("\t%s\n", (state == T) ? "true" : "false");
			state = 0;
		}
	} while(c != EOF);
	return 0;
}
