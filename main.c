#include <stdio.h>
enum Q {q0=0, T=13, F=14 };
char delta[13][12] = {
	/*0 17 89 ad  e  f  x +- ' ' .  0  F*/
	{ 1, 7, 7, F, F, F ,F ,5, 0,11, F, F},
	{ 6, 6, F, F, F, F, 2, F, 4,11, T, F},
	{ 3, 3, 3, 3, 3, 3, F, F, F, F, F, F},
	{ 3, 3, 3, 3, 3, 3, F, F, 4, F, T, F},
	{ F, F, F, F, F, F, F, F, 4, F, T, F},
	{ 7, 7, 7, F, F, F, F, F, F, 7, F, F},
	{ 6, 6, F, F, F, F, F, F, 4, F, T, F},
	{ 7, 7, 7, F, 8, F, F, F, 4,11, T, F},
	{ 9, 9, 9, F, F, F, F,10, F, F, F, F},
	{ 9, 9, 9, F, F, F, F, F, 4, F, T, F},
	{ 9, 9, 9, F, F, F, F, F, F, F, F, F},
	{12,12,12, F, F, F, F, F, F, F, F, F},
	{12,12,12, F, 8, F, F, F, 4, F, T, F}
};

char sigma(char c)
{
	switch(c) {
		case '0': return 0;
		case '8':
		case '9': return 2;
		case 'a':
		case 'b':
		case 'c':
		case 'd': return 3;
		case 'e': return 4;
		case 'f': return 5;
		case 'x': return 6;
		case '+':
		case '-': return 7;
		case ' ': return 8;
		case '.': return 9;
		case '\0':return 10;
		default: if(c>='1' && c<='7') return 1;
	}
	return 11;
}

int is_number(char *s)
{
	char state = q0;
	for(char* p = s; state <=12; p++)
		state = delta[state][sigma(*p)];
	return state == T;
}

#define CHECK(s) (printf("%15s\b \t-> %s\n", \
		((void*)#s + 1), \
		((is_number(s)) ? "Yes" : "No")))
int main()
{
	CHECK("  001  ");
	CHECK("+3.14159");
	CHECK("0xff");
	CHECK("-0.3e8");
	CHECK(".3");
	CHECK("0xzz");
	CHECK("1234-");
	return 0;
}
