all: a.out
a.out: main.o
clean:
	$(RM) *.o a.out
.PHONY: all clean
